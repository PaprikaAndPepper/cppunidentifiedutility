#include <iostream>
#include <algorithm>
#include "circulararray/circular.hpp"

struct WOW {
  WOW & swap(WOW &){
    return *this;
  }
};

int main(int argc, char ** argv){
  (void)argc; (void)argv;
  CUU::CircularArray<int, 5> foo;//{1,3,4,5,6};
  CUU::CircularArray<int, 5> bar;
  CUU::CircularArray<WOW, 0> bin;
  CUU::CircularArray<double, 1> bon;
  //std::cout << CUU::trait::CircularArray_trait<int, 19>::_B_mov(1, -3) << '\n';
  auto tester = bin.begin();
  bon.fill(-12);
  for(decltype(bon)::size_type i = 0; i < bon.max_size(); ++i){
    std::cout << i << '\t' << bon.data()[i] << '\n';
  }
  bon.push_back(11);
  for(decltype(bon)::size_type i = 0; i < bon.max_size(); ++i){
    std::cout << i << '\t' << bon.data()[i] << '\n';
  }
  bon.push_back(12);
  for(decltype(bon)::size_type i = 0; i < bon.max_size(); ++i){
    std::cout << i << '\t' << bon.data()[i] << '\n';
  }
  bon.push_back(13);
  for(decltype(bon)::size_type i = 0; i < bon.max_size(); ++i){
    std::cout << i << '\t' << bon.data()[i] << '\n';
  }
  (void)tester;
  (void)bin;
  decltype(foo)::iterator owow;
  foo.fill(12);
  bar.fill(7);
  foo.swap(bar);
  for(int i = 1; i < 11; i++){
    foo.push_back(i);
  }
  std::for_each(foo.begin(), foo.end(), [](int & a){a *= 5;});
  bar.push_back(11);
  bar.push_back(-11);
  for(std::size_t i = 0; i < foo.size(); ++i){
    std::cout << "Foo : " << foo.data()[i] << "\t Bar : " << bar.data()[i] << '\n';
  }
  // std::cout << (foo == bar ? "UO" : "NO") << '\n';
  // std::cout << (foo != bar ? "UO" : "NO") << '\n';
  std::cout << (foo.begin() == foo.end() ? "UO" : "NO") << '\n';
  for(auto && i : foo){
    i -= 3;
  }
  for(auto && i : foo){
    std::cout << i << '\t';
  }
  std::cout << '\n';
  // for(auto i = foo.crbegin(); i != foo.crend(); ++i){
  //   std::cout << *i << '\n';
  // }
  // for(auto i = foo.cbegin(); i != foo.cend(); ++i){
  //   std::cout << *i << '\n';
  // }
  return 0;
}
