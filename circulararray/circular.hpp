// <CircularArray> -*-C++-*-
/*=================================================================================+
 |Copyright 2019 PaprikaAndPepper                                                  |
 |                                                                                 |
 |Permission is hereby granted, free of charge, to any person obtaining a copy of  |
 |this software and associated documentation files (the "Software"), to deal in the|
 |Software without restriction, including without limitation the rights to use,    |
 |copy, modify, merge, publish, distribute, sublicense, and/or sell copies of      |
 |the Software, and to permit persons to whom the Software is furnished to do so,  |
 |subject to the following conditions:                                             |
 |                                                                                 |
 |The above copyright notice and this permission notice shall be included in all   |
 |copies or substantial portions of the Software.                                  |
 |                                                                                 |
 |THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR       |
 |IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS |
 |FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR   |
 |COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER   |
 |IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN          |
 |CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
 +=================================================================================*/
#ifndef CUU_CIRCULAR_ARRAY_HEAD_HPP
#define CUU_CIRCULAR_ARRAY_HEAD_HPP
#include <cstddef>
#include <iterator>

namespace CUU {
  namespace trait{
    template<typename T,
             std::size_t SZ>
    struct CircularArray_trait{
      using ca_trait = T[SZ];
      static constexpr auto _S_ptr(ca_trait const & __in) noexcept -> T* {
        return const_cast<T*>(__in);
      }
      static constexpr auto _S_ref(ca_trait const & __in, std::size_t __offs) noexcept -> T* {
        return const_cast<T&>(__in[__offs]);
      }
      static constexpr auto _B_norm(std::size_t __border) noexcept -> std::size_t{
        return __border;
      }
    };
    template<typename T>
    struct CircularArray_trait<T,0>{
      struct ca_trait{};
      static constexpr auto _S_ptr(ca_trait &) noexcept -> T* {
        return nullptr;
      }
      static constexpr auto _S_ref(ca_trait &, std::size_t) noexcept -> T* {
        return *static_cast<T*>(nullptr);
      }
      static constexpr auto _B_norm(std::size_t) noexcept -> std::size_t{
        return 0;
      }
    };
  }
  template<typename T,
           std::size_t SIZE,
           std::size_t SZ = (SIZE == 0 ? 0 : SIZE + 1)>
  class CircularArray{
    template<typename Ti = T>
    class Iter {
    public:
      using storage_type = typename trait::CircularArray_trait<T, SZ>;
      using iterator_category = std::bidirectional_iterator_tag;
      using difference_type = std::ptrdiff_t;
      using value_type = Ti;
      using pointer =  value_type *;
      using reference = value_type &;
    private:
      std::size_t _current;
      pointer _data;
    public:
      constexpr Iter(pointer ptr = nullptr, std::size_t i = 0): _current{i}, _data{ptr}{}
      constexpr Iter(Iter const & i): _current{i._current}, _data{i._data}{}
      constexpr Iter(Iter && i): _current{i._current}, _data{i._data}{}
      constexpr auto operator=(Iter const & i) noexcept -> Iter &{
        this->_current = i._current;
        this->_data = i._data;
        return *this;
      }
      constexpr auto operator=(Iter && i) noexcept -> Iter &{
        this->_current = i._current;
        this->_data = i._data;
        return *this;
      }
      //Member access
      constexpr auto operator*() const noexcept -> reference {
        return _data[_current];
      }
      constexpr auto operator->() const noexcept -> reference {
        return _data[_current];
      }
      //Legacy Input operator
      constexpr auto operator++() noexcept -> Iter &{
        if(++_current == SZ){
          _current = 0;
        }
        return *this;
      }
      //Legacy Forward operator
      constexpr auto operator++(int) noexcept -> Iter{
        Iter temp{*this};
        if(++_current == SZ){
          _current = 0;
        }
        return temp;
      }
      //Legacy Bidirectional operator
      constexpr auto operator--() noexcept -> Iter &{
        if(_current == 0){
          _current = SZ;
        }
        --_current;
        return *this;
      }
      constexpr auto operator--(int) noexcept -> Iter{
        Iter temp{*this};
        if(_current == 0){
          _current = SZ;
        }
        --_current;
        return temp;
      }
      //Friend
      //Legacy Input operator
      friend constexpr auto operator==(Iter const & lhs, Iter const & rhs) noexcept -> bool{
        return lhs._current == rhs._current;
      }
      friend constexpr auto operator!=(Iter const & lhs, Iter const & rhs) noexcept -> bool{
        return !(lhs == rhs);
      }
    };
  public:
    using storage_type = typename trait::CircularArray_trait<T, SZ>;
    using data_type = typename storage_type::ca_trait;
    using value_type = T;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = value_type &;
    using const_reference = value_type const &;
    using pointer = value_type *;
    using const_pointer = value_type const *;
    using iterator = Iter<T>;
    using const_iterator = Iter<T const>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    //Iterator
  private:
    data_type _data;
    std::size_t _border = (SZ - 1);
  public:
    constexpr auto push_back(const_reference i) -> CircularArray & {
      this->_data[_border] = i;
      this->_border = (_border == 0 ? (SZ - 1) : _border -1);
      return *this;
    }

    //Iterator Access
    constexpr auto begin() -> iterator {
      return iterator{storage_type::_S_ptr(_data), storage_type::_B_norm(_border)};
    }
    constexpr auto end() -> iterator {
      auto const in = storage_type::_B_norm(_border);
      return iterator{storage_type::_S_ptr(_data),(in == 0 ? SZ : in) -1};
    }
    constexpr auto cbegin() const -> const_iterator {
      return const_iterator{storage_type::_S_ptr(_data),storage_type::_B_norm(_border)};
    }
    constexpr auto cend() const -> const_iterator {
      auto const in = storage_type::_B_norm(_border);
      return const_iterator{storage_type::_S_ptr(_data),(in == 0 ? SZ : in) -1};
    }
    constexpr auto begin() const -> const_iterator {
      return this->cbegin();
    }
    constexpr auto end() const -> const_iterator {
      return this->cend();
    }
    //Reverse iterator
    constexpr auto rbegin() -> reverse_iterator {
      return reverse_iterator{this->end()};
    }
    constexpr auto rend() -> reverse_iterator {
      return reverse_iterator{this->begin()};
    }
    constexpr auto crbegin() const -> const_reverse_iterator {
      return const_reverse_iterator{this->cend()};
    }
    constexpr auto crend() const -> const_reverse_iterator {
      return const_reverse_iterator{this->cbegin()};
    }
    constexpr auto rbegin() const -> const_reverse_iterator {
      return this->crbegin();
    }
    constexpr auto rend() const -> const_reverse_iterator {
      return this->crend();
    }
    //Access
    constexpr auto data() noexcept -> pointer {
      return storage_type::_S_ptr(_data);
    }
    constexpr auto data() const noexcept -> const_pointer {
      return storage_type::_S_ptr(_data);
    }
    //Operation
    constexpr auto fill(const_reference value) noexcept -> void {
      for(size_type i = 0; i < SZ; ++i){
        if(i != _border){
          _data[i] = value;
        }
      }
    }
    constexpr auto swap(CircularArray & other) -> void {
      std::swap(this->_data, other._data);
    }
    //Capacity Function
    //Max_size is the raw value of number of used data
    constexpr auto max_size() const noexcept -> size_type {
      return SZ;
    }
    constexpr auto size() const noexcept -> size_type {
      return SIZE;
    }
    constexpr auto empty() const noexcept -> bool {
      return SIZE == 0 ? true : false;
    }
    //NOTE: SHOULD NOT DECLARE AND IMPLEMENT CONSTRUCTOR, ASSIGNMENT, AND DESTRUCTOR
    //Friend operator
    friend constexpr auto operator==(CircularArray<T, SZ> const & lhs,
                                     CircularArray<T, SZ> const & rhs ) -> bool{
      if(lhs._border != rhs._border){
        return false;
      }
      for(size_type i = 0; i < SZ; ++i){
        if((i != lhs._border) && !(lhs._data[i] == rhs._data[i])){
          return false;
        }
      }
      return true;
    }
    friend constexpr auto operator!=(CircularArray<T, SZ> const & lhs,
                                     CircularArray<T, SZ> const & rhs ) -> bool{
      return !(lhs == rhs);
    }
  };
}

#endif //CUU_CIRCULAR_ARRAY_HEAD_HPP
